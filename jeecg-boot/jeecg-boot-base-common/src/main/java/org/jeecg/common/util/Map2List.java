package org.jeecg.common.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/25
 */
public class Map2List {
    /**
     * 将Map转换为JavaBean
     *
     * @param map
     * @param obj
     * @return
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static Object mapToBean(Map<String, Object> map, Object obj) throws NoSuchMethodException, SecurityException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        // 获取JavaBean中的所有属性
        Field[] field = obj.getClass().getDeclaredFields();

        for (Field fi : field) {
            // 判断key值是否存在
            if (map.containsKey(fi.getName())) {
                // 获取key的value值
                String value = map.get(fi.getName()).toString();
                // 将属性的第一个字母转换为大写
                String frist = fi.getName().substring(0, 1).toUpperCase();
                // 属性封装set方法
                String setter = "set" + frist + fi.getName().substring(1);
                // 获取当前属性类型
                Class<?> type = fi.getType();
                // 获取JavaBean的方法,并设置类型
                Method method = obj.getClass().getMethod(setter, type);

                // 判断属性为double类型
                if ("java.lang.String".equals(type.getName())) {

                    // 调用当前Javabean中set方法，并传入指定类型参数
                    method.invoke(obj, value);

                } else if ("int".equals(type.getName())) {

                    method.invoke(obj, Integer.parseInt(value));

                }else if ("double".equals(type.getName())) {

                    method.invoke(obj, Double.valueOf(value));


                } else if ("char".equals(type.getName())) {

                    method.invoke(obj, value);

                }
            }
        }

        return obj;
    }

    /**
     * 将List<Map<String,Object>>转换成List<javaBean>
     *
     * @param listm
     * @param obj
     * @return
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws NoSuchMethodException
     */
    public static List<Object> ListMapToListBean(List<Map<String, Object>> listm, Object obj)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {

        List<Object> list = new ArrayList<Object>();
        // 循环遍历出map对象
        for (Map<String, Object> m : listm) {
            // 调用将map转换为JavaBean的方法
            Object objs = mapToBean(m, obj);
            // 添加进list集合
            list.add(objs);
        }

        return list;
    }

}
