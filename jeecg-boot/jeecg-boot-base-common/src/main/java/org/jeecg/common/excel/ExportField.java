package org.jeecg.common.excel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/25
 */
@Data
public class ExportField {

    @ApiModelProperty("实体映射的字段")
    private String entityAttrName;

    @ApiModelProperty("导出字段,中文描述")
    private String filedChineseName;

    @ApiModelProperty("合并单元格")
    private List<ExportField> children;
}
