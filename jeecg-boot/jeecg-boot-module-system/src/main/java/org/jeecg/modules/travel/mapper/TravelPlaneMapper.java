package org.jeecg.modules.travel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.travel.entity.TravelPlane;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.travel.entity.TravelTrain;

/**
 * @Description: 飞机差旅标准
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
public interface TravelPlaneMapper extends BaseMapper<TravelPlane> {

    public List<TravelPlane> selectByPostCode(@Param("postCode") String postCode);
}
