package org.jeecg.modules.salary.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.salary.entity.SysSalary;
import org.jeecg.modules.salary.mapper.SysSalaryMapper;
import org.jeecg.modules.salary.service.ISysSalaryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 薪资表
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
@Service
public class SysSalaryServiceImpl extends ServiceImpl<SysSalaryMapper, SysSalary> implements ISysSalaryService {

    @Override
    public SysSalary getByUserId(String userId,String reportId) {
        LambdaQueryWrapper<SysSalary> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(SysSalary::getUserId,userId);
        queryWrapper.eq(SysSalary::getReportId,reportId);
        return this.getOne(queryWrapper);
    }

    @Override
    public List<SysSalary> selectByUserId(String userId) {
        LambdaQueryWrapper<SysSalary> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(SysSalary::getUserId,userId);
        return this.list(queryWrapper);
    }
}
