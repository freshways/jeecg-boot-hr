package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.TravelHotel;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.travel.vo.MyHotelVo;

import java.util.List;
import java.util.Map;

/**
 * @Description: 酒店差标
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface ITravelHotelService extends IService<TravelHotel> {
    List<Map<Object,Object>> getMyHotel();
}
