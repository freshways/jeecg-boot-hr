package org.jeecg.modules.travel.service.impl;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.travel.entity.TravelHotel;
import org.jeecg.modules.travel.mapper.TravelHotelMapper;
import org.jeecg.modules.travel.service.ITravelHotelService;
import org.jeecg.modules.travel.vo.MyHotelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * @Description: 酒店差标
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Service
public class TravelHotelServiceImpl extends ServiceImpl<TravelHotelMapper, TravelHotel> implements ITravelHotelService {

    @Autowired
    private TravelHotelMapper travelHotelMapper;

    @Override
    public List<Map<Object,Object>> getMyHotel() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //获取职务编码
        String postCode = sysUser.getPost();
        List<Map<Object,Object>> travelHotels = travelHotelMapper.selectHotelVoByPostCode(postCode);
        return travelHotels;
    }
}
