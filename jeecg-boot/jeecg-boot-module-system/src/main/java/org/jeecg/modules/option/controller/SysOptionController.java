package org.jeecg.modules.option.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.option.entity.SysOption;
import org.jeecg.modules.option.service.ISysOptionService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.vo.AdjustVo;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysUserService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 期权管理
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
@Api(tags="期权管理")
@RestController
@RequestMapping("/option/sysOption")
@Slf4j
public class SysOptionController extends JeecgController<SysOption, ISysOptionService> {
	@Autowired
	private ISysOptionService sysOptionService;

	@Autowired
	private ISysUserService  sysUserService;
	
	/**
	 * 分页列表查询
	 *
	 * @param sysOption
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "期权管理-分页列表查询")
	@ApiOperation(value="期权管理-分页列表查询", notes="期权管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SysOption sysOption,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SysOption> queryWrapper = QueryGenerator.initQueryWrapper(sysOption, req.getParameterMap());
		Page<SysOption> page = new Page<SysOption>(pageNo, pageSize);
		IPage<SysOption> pageList = sysOptionService.page(page, queryWrapper);
		return Result.ok(pageList);
	}


	 /**
	  * 分页列表查询
	  *
	  * @param sysOption
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "期权管理-我的分页列表查询")
	 @ApiOperation(value="期权管理-我的分页列表查询", notes="期权管理-我的分页列表查询")
	 @GetMapping(value = "/MyList")
	 public Result<?> queryPageMyList(SysOption sysOption,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 QueryWrapper<SysOption> queryWrapper = QueryGenerator.initQueryWrapper(sysOption, req.getParameterMap());
		 queryWrapper.eq("user_id",sysUser.getId());
		 Page<SysOption> page = new Page<SysOption>(pageNo, pageSize);
		 IPage<SysOption> pageList = sysOptionService.page(page, queryWrapper);
		 return Result.ok(pageList);
	 }

	/**
	 *   添加
	 *
	 * @param sysOption
	 * @return
	 */
	@AutoLog(value = "期权管理-添加")
	@ApiOperation(value="期权管理-添加", notes="期权管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SysOption sysOption) {

		sysOption.setActualRatio(sysOption.getFixedRatio());
		sysOption.setStatus(CommonConstant.STATUS_0);
		sysOptionService.save(sysOption);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param sysOption
	 * @return
	 */
	@AutoLog(value = "期权管理-编辑")
	@ApiOperation(value="期权管理-编辑", notes="期权管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SysOption sysOption) {
		sysOption.setActualRatio(sysOption.getFixedRatio());
		sysOptionService.updateById(sysOption);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "期权管理-通过id删除")
	@ApiOperation(value="期权管理-通过id删除", notes="期权管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		sysOptionService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "期权管理-批量删除")
	@ApiOperation(value="期权管理-批量删除", notes="期权管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.sysOptionService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "期权管理-通过id查询")
	@ApiOperation(value="期权管理-通过id查询", notes="期权管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SysOption sysOption = sysOptionService.getById(id);
		if(sysOption==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(sysOption);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param sysOption
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SysOption sysOption) {
        return super.exportXls(request, sysOption, SysOption.class, "期权管理");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SysOption.class);
    }


	 @AutoLog(value = "期权管理-返回字段值")
	 @ApiOperation(value="期权管理-返回字段值", notes="期权管理-返回字段值")
	 @GetMapping(value = "/dicCode")
	 public Result<?>  dicCode() {
		 List<AdjustVo> list = new ArrayList<>();
		 LambdaQueryWrapper<SysUser> queryWrapper = new  LambdaQueryWrapper();
		 queryWrapper.eq(SysUser::getStatus,CommonConstant.STATUS_1);
		 queryWrapper.eq(SysUser::getDelFlag,CommonConstant.STATUS_0);
		 List<SysUser> userList = sysUserService.list(queryWrapper);
		 for(SysUser user:userList){
			 AdjustVo adjustVo = new AdjustVo();
			 adjustVo.setValue(user.getId());
			 adjustVo.setText(user.getRealname());
			 adjustVo.setTitle(user.getRealname());
			 list.add(adjustVo);
		 }
		 return Result.ok(list);
	 }


 }
