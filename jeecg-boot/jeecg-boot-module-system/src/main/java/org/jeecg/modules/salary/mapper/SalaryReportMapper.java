package org.jeecg.modules.salary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.salary.entity.SalaryReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.salary.vo.DataVo;

/**
 * @Description: 薪资报表
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
public interface SalaryReportMapper extends BaseMapper<SalaryReport> {

    @Select(value = " SELECT\n" +
            "\tsu.username ,\n" +
            "\tsd.depart_name  departName\n" +
            "FROM\n" +
            "\tsys_user su\n" +
            "\tLEFT JOIN sys_user_depart sud ON su.id = sud.user_id\n" +
            "\tLEFT JOIN sys_depart sd ON sud.dep_id = sd.id where su.del_flag ='0' ")
    List<DataVo> getList();
}
