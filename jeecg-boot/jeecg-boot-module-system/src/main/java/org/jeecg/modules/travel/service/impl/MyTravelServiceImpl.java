package org.jeecg.modules.travel.service.impl;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.travel.entity.TravelHotel;
import org.jeecg.modules.travel.entity.TravelTrain;
import org.jeecg.modules.travel.mapper.SysAreaCityMapper;
import org.jeecg.modules.travel.mapper.SysAreasMapper;
import org.jeecg.modules.travel.mapper.TravelHotelMapper;
import org.jeecg.modules.travel.mapper.TravelTrainMapper;
import org.jeecg.modules.travel.service.IMyTravelService;
import org.jeecg.modules.travel.vo.MyTravelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
@Service
public class MyTravelServiceImpl implements IMyTravelService {
    @Autowired
    private SysAreasMapper sysAreasMapper;
    @Autowired
    private SysAreaCityMapper sysAreaCityMapper;
    @Autowired
    private TravelHotelMapper travelHotelMapper;
    @Autowired
    private TravelTrainMapper travelTrainMapper;

    @Override
    public MyTravelVo getMyTravel() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //获取职务编码
        String postCode = sysUser.getPost();
        MyTravelVo vo = new MyTravelVo();
        //根据职务编码获取火车差标和酒店差标
        List<TravelTrain> travelTrains = travelTrainMapper.selectByPostCode(postCode);

        vo.setTravelTrains(travelTrains);
        List<TravelHotel> travelHotels = travelHotelMapper.selectByPostCode(postCode);

        vo.setTravelHotels(travelHotels);
        return vo;
    }
}
