package org.jeecg.modules.travel.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.system.service.ISysDictService;
import org.jeecg.modules.travel.entity.TravelPlane;
import org.jeecg.modules.travel.entity.TravelTrain;
import org.jeecg.modules.travel.mapper.TravelPlaneMapper;
import org.jeecg.modules.travel.mapper.TravelTrainMapper;
import org.jeecg.modules.travel.service.ITravelPlaneService;
import org.jeecg.modules.travel.vo.MyPlaneVo;
import org.jeecg.modules.travel.vo.MyTrainVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 飞机差旅标准
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
@Service
public class TravelPlaneServiceImpl extends ServiceImpl<TravelPlaneMapper, TravelPlane> implements ITravelPlaneService {

    @Autowired
    private TravelPlaneMapper  TravelPlaneMapper;
    @Autowired
    private ISysDictService sysDictService;

    @Override
    public List<MyPlaneVo> getMyPlane() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //获取职务编码
        String postCode = sysUser.getPost();
        List<TravelPlane> travelPlaneList = TravelPlaneMapper.selectByPostCode(postCode);
        //获取席位字典信息
        List<DictModel>  seatsModels = sysDictService.queryDictItemsByCode("aircraft_space");
        Map<String ,String> seatsMap = new HashMap<String,String>();
        for (DictModel model: seatsModels            ) {
            seatsMap.put(model.getValue(),model.getText());
        }

        List<MyPlaneVo> vos =  new ArrayList<>();
        for (TravelPlane travelPlane: travelPlaneList            ) {
            MyPlaneVo vo = new MyPlaneVo();
            String str = travelPlane.getLevel();
            String[] seats = str.split(",");
            List<String> list = new ArrayList<>();
            for (String seat:seats        ) {
                list.add(seatsMap.get(seat));
            }
            vo.setLevel(StringUtils.join(list,","));
            vos.add(vo);
        }
        return vos;
    }
}
