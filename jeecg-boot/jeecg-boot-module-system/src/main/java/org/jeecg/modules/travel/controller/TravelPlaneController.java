package org.jeecg.modules.travel.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.travel.entity.TravelPlane;
import org.jeecg.modules.travel.service.ITravelPlaneService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 飞机差旅标准
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
@Api(tags="飞机差旅标准")
@RestController
@RequestMapping("/travel/travelPlane")
@Slf4j
public class TravelPlaneController extends JeecgController<TravelPlane, ITravelPlaneService> {
	@Autowired
	private ITravelPlaneService travelPlaneService;
	
	/**
	 * 分页列表查询
	 *
	 * @param travelPlane
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-分页列表查询")
	@ApiOperation(value="飞机差旅标准-分页列表查询", notes="飞机差旅标准-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TravelPlane travelPlane,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TravelPlane> queryWrapper = QueryGenerator.initQueryWrapper(travelPlane, req.getParameterMap());
		Page<TravelPlane> page = new Page<TravelPlane>(pageNo, pageSize);
		IPage<TravelPlane> pageList = travelPlaneService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param travelPlane
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-添加")
	@ApiOperation(value="飞机差旅标准-添加", notes="飞机差旅标准-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TravelPlane travelPlane) {
		travelPlaneService.save(travelPlane);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param travelPlane
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-编辑")
	@ApiOperation(value="飞机差旅标准-编辑", notes="飞机差旅标准-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TravelPlane travelPlane) {
		travelPlaneService.updateById(travelPlane);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-通过id删除")
	@ApiOperation(value="飞机差旅标准-通过id删除", notes="飞机差旅标准-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		travelPlaneService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-批量删除")
	@ApiOperation(value="飞机差旅标准-批量删除", notes="飞机差旅标准-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.travelPlaneService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "飞机差旅标准-通过id查询")
	@ApiOperation(value="飞机差旅标准-通过id查询", notes="飞机差旅标准-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TravelPlane travelPlane = travelPlaneService.getById(id);
		if(travelPlane==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(travelPlane);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param travelPlane
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TravelPlane travelPlane) {
        return super.exportXls(request, travelPlane, TravelPlane.class, "飞机差旅标准");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TravelPlane.class);
    }

}
