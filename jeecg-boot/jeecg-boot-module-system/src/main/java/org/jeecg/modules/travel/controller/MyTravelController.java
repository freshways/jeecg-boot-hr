package org.jeecg.modules.travel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.travel.entity.SysAreas;
import org.jeecg.modules.travel.entity.TravelTrain;
import org.jeecg.modules.travel.service.IMyTravelService;
import org.jeecg.modules.travel.service.ITravelHotelService;
import org.jeecg.modules.travel.service.ITravelPlaneService;
import org.jeecg.modules.travel.service.ITravelTrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
@RestController
@RequestMapping("/travel/my")
@Slf4j
public class MyTravelController {

    @Autowired
    private ITravelTrainService travelTrainService;
    @Autowired
    private ITravelHotelService travelHotelService;

    @Autowired
    private ITravelPlaneService travePlaneService;

    @Autowired
    private IMyTravelService myTravelService;

    @AutoLog(value = "我的差标查询")
    @ApiOperation(value="我的差标-信息查询查询", notes="我的差标-信息查询查询")
    @GetMapping(value = "/")
    public Result<?> queryMyTravel() {

        return Result.ok(myTravelService.getMyTravel());
    }


    @AutoLog(value = "我的火车差标查询")
    @ApiOperation(value="我的火车差标-信息查询查询", notes="我的火车差标-信息查询查询")
    @GetMapping(value = "/train")
    public Result<?> queryMyTravelTrain() {
        List list = travelTrainService.getMyTrains();
        return Result.ok(list);
    }

    @AutoLog(value = "我的酒店差标查询")
    @ApiOperation(value="我的酒店差标-信息查询查询", notes="我的酒店差标-信息查询查询")
    @GetMapping(value = "/hotel")
    public Result<?> queryMyTravelHotel() {
        List list = travelHotelService.getMyHotel();
        return Result.ok(list);
    }


    @AutoLog(value = "我的飞机差标查询")
    @ApiOperation(value="我的飞机差标-信息查询查询", notes="我的飞机差标-信息查询查询")
    @GetMapping(value = "/plane")
    public Result<?> queryMyTravelPlane() {
        List list = travePlaneService.getMyPlane();
        return Result.ok(list);
    }
}
