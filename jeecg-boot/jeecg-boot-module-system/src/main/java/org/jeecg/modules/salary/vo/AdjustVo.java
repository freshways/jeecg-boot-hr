package org.jeecg.modules.salary.vo;

import lombok.Data;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/9/2
 */
@Data
public class AdjustVo {

    private  String value;
    private  String text;
    private String title;

}
