package org.jeecg.modules.salary.service;

import org.jeecg.modules.salary.entity.Salary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工资信息
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface ISalaryService extends IService<Salary> {

    Salary getMySalary();
}
