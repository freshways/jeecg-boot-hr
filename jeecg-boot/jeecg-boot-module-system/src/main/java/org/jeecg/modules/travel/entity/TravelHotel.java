package org.jeecg.modules.travel.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 酒店差标
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Data
@TableName("travel_hotel")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="travel_hotel对象", description="酒店差标")
public class TravelHotel implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**职级*/
	@Excel(name = "职级", width = 15, dictTable = "sys_position", dicText = "name", dicCode = "code")
	@Dict(dictTable = "sys_position", dicText = "name", dicCode = "code")
    @ApiModelProperty(value = "职级")
    private String position;
	/**地区类别*/
	@Excel(name = "地区类别", width = 15, dictTable = "sys_areas", dicText = "area_type", dicCode = "id")
	@Dict(dictTable = "sys_areas", dicText = "area_type", dicCode = "id")
    @ApiModelProperty(value = "地区类别")
    private String area;
	/**平均每晚房费上限*/
	@Excel(name = "平均每晚房费上限", width = 15)
    @ApiModelProperty(value = "平均每晚房费上限")
    private BigDecimal max;
	/**酒店星级*/
	@Excel(name = "酒店星级", width = 15, dicCode = "hotel_star")
	@Dict(dicCode = "hotel_star")
    @ApiModelProperty(value = "酒店星级")
    private String hotelStar;
	/**超标管控*/
	@Excel(name = "超标管控", width = 15, dicCode = "controls")
	@Dict(dicCode = "controls")
    @ApiModelProperty(value = "超标管控")
    private String controls;
}
