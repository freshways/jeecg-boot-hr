package org.jeecg.modules.salary.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/19
 */
public interface SalarySubMapper {



    /**
     * 插入字段
     *
     * @param fieldName
     * @return
     */
    int insertFields(@Param("fieldName") String fieldName);

    /**
     * 删除库表字段
     * @param fieldName
     * @return
     */
    int deleteFields(@Param("fieldName") String fieldName);


    /**
     *
     * @param keys
     * @return
     */
    int updateSalarySub(Map keys);

    /**
     *
     * @param keys
     * @return
     */
    int insertSalarySub(Map keys);

    /**
     * 根据ID查询录入数据
     * @param id
     * @return
     */
    @Select(value = "select * from salary_sub where id = #{id}")
    Map<String, Object> getById(String id);


    /**
     * 查询全部数据
     * @return
     */
    @Select(value = " select * from salary_sub ")
    List<HashMap<String,Object>> getAll();

    /**
     * 分页
     * @param currIndex 开始页
     * @param lastIndex 结束页
     * @return
     */
    List<HashMap<String,Object>> getPage(@Param("userId") String userId,@Param("realname") String realname,
                                         @Param("currIndex") int currIndex, @Param("lastIndex") int lastIndex);


    /**
     * 分页
     * @param currIndex 开始页
     * @param lastIndex 结束页
     * @return
     */
    List<Map<String,Object>> getReportPage(@Param("realname") String realname,@Param("reportId") String reportId,
                                           @Param("currIndex") int currIndex, @Param("lastIndex") int lastIndex);


    /**
     *  查询上报数据
     * @param userId 用户ID
     * @param realname 用户名
     * @return
     */
    List<Map<String,Object>> getList(@Param("userId") String userId,@Param("realname") String realname);

    /**
     * 字段求和
     * @param fieldCode
     * @return
     */
    String getSum(String fieldCode);

}
