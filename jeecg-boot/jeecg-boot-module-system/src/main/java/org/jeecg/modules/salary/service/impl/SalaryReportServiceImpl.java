package org.jeecg.modules.salary.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.entity.SalaryReport;
import org.jeecg.modules.salary.mapper.SalaryReportMapper;
import org.jeecg.modules.salary.mapper.SalarySubMapper;
import org.jeecg.modules.salary.service.ISalaryFieldsService;
import org.jeecg.modules.salary.service.ISalaryReportService;
import org.jeecg.modules.salary.vo.DataVo;
import org.jeecg.modules.system.mapper.SysPositionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 薪资报表
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
@Service
public class SalaryReportServiceImpl extends ServiceImpl<SalaryReportMapper, SalaryReport> implements ISalaryReportService {

    @Autowired
    ISalaryFieldsService salaryFieldsService;

    @Autowired
    SalarySubMapper salarySubMapper;

    @Autowired
    SysPositionMapper sysPositionMapper;

    @Autowired
    SalaryReportMapper salaryReportMapper;

    @Override
    public void editStatus() {
        Date date = new Date();
        LambdaQueryWrapper<SalaryReport> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SalaryReport::getStatus, CommonConstant.STATUS_1);
        List<SalaryReport> salaryReportList = this.list(queryWrapper);
        for(SalaryReport salaryReport : salaryReportList){
            Date endDate = salaryReport.getEndDate();
            if(endDate.getTime()<date.getTime()){
                salaryReport.setStatus(CommonConstant.STATUS_2);
                updateById(salaryReport);
            }
        }
    }

    @Override
    public List<SalaryReport> getList() {
        LambdaQueryWrapper<SalaryReport> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SalaryReport::getStatus, CommonConstant.STATUS_1);
        List<SalaryReport> salaryReportList = this.list(queryWrapper);
        return salaryReportList;
    }

    @Override
    public JSONObject reportHead() {
        JSONObject object = new JSONObject();
        //处理表头字段
        JSONArray jsonArray = new  JSONArray();

        JSONObject object0 = new JSONObject();
        object0.put("id","id");
        object0.put("fieldCode","dept_name");
        object0.put("fieldName","部门");
        jsonArray.add(object0);

        JSONObject object1 = new JSONObject();
        object1.put("id","id");
        object1.put("fieldCode","user_name");
        object1.put("fieldName","姓名");
        jsonArray.add(object1);

        JSONObject object2 = new JSONObject();
        object2.put("id","id");
        object2.put("fieldCode","position");
        object2.put("fieldName","职务");
        jsonArray.add(object2);
        //获取薪资管理
        List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
        for(SalaryFields salaryFields : salaryFieldsList){
            JSONObject obj = new JSONObject();
            obj.put("id",salaryFields.getId());
            obj.put("fieldCode",salaryFields.getFieldCode());
            obj.put("fieldName",salaryFields.getFieldName());
            //查询是否含有子类
            List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
            if (salaryFieldsList1.size()>0) {
                JSONArray jsonArray1 = new  JSONArray();
                for(SalaryFields salaryFields1 : salaryFieldsList1){
                    JSONObject objj = new JSONObject();
                    objj.put("id",salaryFields1.getId());
                    objj.put("fieldCode",salaryFields1.getFieldCode());
                    objj.put("fieldName",salaryFields1.getFieldName());
                    jsonArray1.add(objj);
                }
                obj.put("children",jsonArray1);
            }
            jsonArray.add(obj);
        }
        object.put("cloumns",jsonArray);
        return object;
    }



    @Override
    public List<Map<String, Object>> getPage(String realname,String reportId,int currPage, int pageSize) {

        //从第几条数据开始
        int firstIndex = (currPage - 1) * pageSize;
        //到第几条数据结束
        int lastIndex = currPage * pageSize;

        List<Map<String, String>> mapList = sysPositionMapper.getPositionList();

        List<Map<String, Object>> reportPage = salarySubMapper.getReportPage(realname,reportId,firstIndex,lastIndex);


        List<DataVo> dataVoList = salaryReportMapper.getList();

        for(Map map : reportPage){
            String deptName = "";
            for(Map map1:mapList){
                if(map.get("user_id").equals(map1.get("username"))){
                    map.put("position",map1.get("name"));
                    break;
                }
            }
            for(DataVo dataVo:dataVoList){
                 if(map.get("user_id").equals(dataVo.getUsername())){
                     deptName += dataVo.getDepartName()+",";
                 }
            }
            if(StringUtils.isNotEmpty(deptName)){
                deptName = deptName.substring(0,deptName.length()-1);
            }
            map.put("dept_name",deptName);
        }
        return reportPage;
    }
}
