package org.jeecg.modules.salary.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 工资信息
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Data
@TableName("salary")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="salary对象", description="工资信息")
public class Salary implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**员工姓名*/
	@Excel(name = "员工姓名", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "员工姓名")
    private String userId;
	/**基本工资*/
	@Excel(name = "基本工资", width = 15)
    @ApiModelProperty(value = "基本工资")
    private Double jbgz;
	/**岗位工资*/
	@Excel(name = "岗位工资", width = 15)
    @ApiModelProperty(value = "岗位工资")
    private Double gwgz;
	/**伙食补贴*/
	@Excel(name = "伙食补贴", width = 15)
    @ApiModelProperty(value = "伙食补贴")
    private Double hsbt;
	/**绩效工资*/
	@Excel(name = "绩效工资", width = 15)
    @ApiModelProperty(value = "绩效工资")
    private Double jxgz;
	/**五险一金*/
	@Excel(name = "五险一金", width = 15)
    @ApiModelProperty(value = "五险一金")
    private Double wxyj;
	/**电脑补贴*/
	@Excel(name = "电脑补贴", width = 15)
    @ApiModelProperty(value = "电脑补贴")
    private Double dnbt;
	/**合计工资*/
	@Excel(name = "合计工资", width = 15)
    @ApiModelProperty(value = "合计工资")
    private Double sfgz;
	/**期权份额*/
	@Excel(name = "期权份额", width = 15)
    @ApiModelProperty(value = "期权份额")
    private String options;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String mark;
}
