package org.jeecg.modules.salary.service;

import org.jeecg.modules.salary.entity.SalaryAdjust;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 薪资调整
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
public interface ISalaryAdjustService extends IService<SalaryAdjust> {

    String getMaxMoney(String userId,String fieldid);
}
