package org.jeecg.modules.travel.vo;

import lombok.Data;
import org.jeecg.modules.travel.entity.TravelHotel;
import org.jeecg.modules.travel.entity.TravelTrain;

import java.util.List;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
@Data
public class MyTravelVo {

    private List<TravelHotel> travelHotels;
    private List<TravelTrain> travelTrains;

}
