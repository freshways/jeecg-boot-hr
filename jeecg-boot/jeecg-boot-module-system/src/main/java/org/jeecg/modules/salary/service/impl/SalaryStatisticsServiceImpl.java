package org.jeecg.modules.salary.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.mapper.SalarySubMapper;
import org.jeecg.modules.salary.service.ISalaryFieldsService;
import org.jeecg.modules.salary.service.ISalaryStatisticsService;
import org.jeecg.modules.salary.service.ISalarySubService;
import org.jeecg.modules.salary.service.ISysSalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/21
 */
@Service
public class SalaryStatisticsServiceImpl implements ISalaryStatisticsService {

    @Autowired
    ISalarySubService salarySubService;

    @Autowired
    ISalaryFieldsService salaryFieldsService;

    @Autowired
    ISysSalaryService sysSalary;

    @Autowired
    SalarySubMapper salarySubMapper;

    @Override
    public JSONObject mySalaryStaticHead(String userId) {
        JSONObject object = new JSONObject();
        //处理表头字段
        JSONArray jsonArray = new  JSONArray();
        if(StringUtils.isEmpty(userId)){
            JSONObject object0 = new JSONObject();
            object0.put("id","id");
            object0.put("fieldCode","user_name");
            object0.put("fieldName","姓名");
            jsonArray.add(object0);
        }
        JSONObject object1 = new JSONObject();
        object1.put("id","id");
        object1.put("fieldCode","sub_date");
        object1.put("fieldName","上报年月");
        jsonArray.add(object1);
        JSONObject object2 = new JSONObject();
        object2.put("id","id");
        object2.put("fieldCode","title");
        object2.put("fieldName","说明");
        jsonArray.add(object2);
        //获取薪资管理
        List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
        for(SalaryFields salaryFields : salaryFieldsList){
            JSONObject obj = new JSONObject();
            obj.put("id",salaryFields.getId());
            obj.put("fieldCode",salaryFields.getFieldCode());
            obj.put("fieldName",salaryFields.getFieldName());
            //查询是否含有子类
            List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
            if (salaryFieldsList1.size()>0) {
                JSONArray jsonArray1 = new  JSONArray();
                for(SalaryFields salaryFields1 : salaryFieldsList1){
                    JSONObject objj = new JSONObject();
                    objj.put("id",salaryFields1.getId());
                    objj.put("fieldCode",salaryFields1.getFieldCode());
                    objj.put("fieldName",salaryFields1.getFieldName());
                    jsonArray1.add(objj);
                }
                obj.put("children",jsonArray1);
            }
            jsonArray.add(obj);
        }
        object.put("cloumns",jsonArray);
        return object;
    }

    @Override
    public List<HashMap<String, Object>> getPage(String userId,String realname,int currPage, int pageSize) {
        //从第几条数据开始
        int firstIndex = (currPage - 1) * pageSize;
        //到第几条数据结束
        int lastIndex = currPage * pageSize;
        //查询数据
        List<HashMap<String, Object>> list = salarySubMapper.getPage(userId,realname,firstIndex,lastIndex);
        return list;
    }

    @Override
    public List<Map<String, Object>> getList(String userId,String realname) {
        return salarySubMapper.getList(userId,realname);
    }

    @Override
    public Map getCount() {
        Map map = new HashMap();
        LambdaQueryWrapper<SalaryFields> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.isNull(SalaryFields::getDeletedAt);
        //是否有子节点
        queryWrapper.eq(SalaryFields::getHasChildren, CommonConstant.STATUS_0);
        queryWrapper.orderByAsc(true,SalaryFields::getFieldSort);
        List<SalaryFields> salaryFieldsList = salaryFieldsService.list(queryWrapper);
        for(SalaryFields salaryFields :salaryFieldsList){
            String count = salarySubMapper.getSum(salaryFields.getFieldCode());
            map.put(salaryFields.getFieldCode(),count);
        }
        return map;
    }
}
