package org.jeecg.modules.travel.service.impl;

import org.jeecg.modules.travel.entity.SysAreaCity;
import org.jeecg.modules.travel.mapper.SysAreaCityMapper;
import org.jeecg.modules.travel.service.ISysAreaCityService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
@Service
public class SysAreaCityServiceImpl extends ServiceImpl<SysAreaCityMapper, SysAreaCity> implements ISysAreaCityService {
	
	@Autowired
	private SysAreaCityMapper sysAreaCityMapper;
	
	@Override
	public List<SysAreaCity> selectByMainId(String mainId) {
		return sysAreaCityMapper.selectByMainId(mainId);
	}
}
