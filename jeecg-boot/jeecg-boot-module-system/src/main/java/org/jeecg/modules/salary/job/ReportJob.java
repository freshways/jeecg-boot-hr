package org.jeecg.modules.salary.job;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.salary.controller.SalaryAdjustController;
import org.jeecg.modules.salary.entity.SalaryReport;
import org.jeecg.modules.salary.service.ISalaryAdjustService;
import org.jeecg.modules.salary.service.ISalaryReportService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/27
 */
@Slf4j
public class ReportJob implements Job {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    ISalaryReportService salaryReportService;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<SalaryReport> salaryReportList = salaryReportService.getList();
        for(SalaryReport salaryReport :salaryReportList){
            Date endDate = salaryReport.getEndDate();
            String from = sdf.format(new Date());
            String to = sdf.format(endDate);
            if(sdf.parse(from).getTime()>sdf.parse(to).getTime()){
                salaryReport.setStatus(CommonConstant.STATUS_2);
                salaryReportService.updateById(salaryReport);
            }
        }
    }

}
