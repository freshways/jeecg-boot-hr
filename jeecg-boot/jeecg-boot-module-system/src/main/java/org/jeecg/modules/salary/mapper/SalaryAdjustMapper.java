package org.jeecg.modules.salary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.salary.entity.SalaryAdjust;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 薪资调整
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
public interface SalaryAdjustMapper extends BaseMapper<SalaryAdjust> {


    @Select(value = " SELECT sa.field_default_new FROM salary_adjust sa WHERE id = ( SELECT max( id ) FROM salary_adjust WHERE  user_id = #{userId} and field_id = #{fieldId} )  ")
    String getMax(@Param(value = "userId") String userId, @Param(value = "fieldId") String fieldId);
}
