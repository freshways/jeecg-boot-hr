package org.jeecg.modules.salary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.salary.entity.SysSalary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 薪资表
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
public interface SysSalaryMapper extends BaseMapper<SysSalary> {

}
