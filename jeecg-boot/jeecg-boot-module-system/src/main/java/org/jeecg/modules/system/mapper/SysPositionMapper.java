package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.system.entity.SysPosition;

import java.util.List;
import java.util.Map;

/**
 * @Description: 职务表
 * @Author: jeecg-boot
 * @Date: 2019-09-19
 * @Version: V1.0
 */
public interface SysPositionMapper extends BaseMapper<SysPosition> {


    @Select(value = " select  su.username,sp.`name` from  sys_user su LEFT JOIN sys_position sp on  su.post = sp.`code` ")
    List<Map<String,String>>  getPositionList();

}
