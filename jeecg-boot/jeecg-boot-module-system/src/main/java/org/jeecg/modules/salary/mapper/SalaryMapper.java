package org.jeecg.modules.salary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.salary.entity.Salary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工资信息
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface SalaryMapper extends BaseMapper<Salary> {
    public Salary getMySalary(@Param("userId") String userId);
}
