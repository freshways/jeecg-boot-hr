package org.jeecg.modules.option.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 期权管理
 * @Author: jeecg-boot
 * @Date:   2020-08-27
 * @Version: V1.0
 */
@Data
@TableName("sys_option")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="sys_option对象", description="期权管理")
public class SysOption implements Serializable {
    private static final long serialVersionUID = 1L;

    /**主键*/
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
    /**创建日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
    /**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    /**更新日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
    /**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**员工名称*/
    @Excel(name = "员工名称", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "员工名称")
    private String userId;
    /**股权数量*/
    @Excel(name = "股权数量", width = 15)
    @ApiModelProperty(value = "股权数量")
    private Integer amount;
    /**行权价格*/
    @Excel(name = "行权价格", width = 15)
    @ApiModelProperty(value = "行权价格")
    private String price;
    /**固定行权比例*/
    @Excel(name = "固定行权比例", width = 15)
    @ApiModelProperty(value = "固定行权比例")
    private String  fixedRatio;
    /**行权期限*/
    @Excel(name = "行权期限", width = 15)
    @ApiModelProperty(value = "行权期限")
    private Integer term;
    /**实际行权比例*/
    @Excel(name = "实际行权比例", width = 15)
    @ApiModelProperty(value = "实际行权比例")
    private String actualRatio;
    /**分配日期*/
    @Excel(name = "分配日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "分配日期")
    private Date assignDate;

    /**状态*/
    @ApiModelProperty(value = "状态")
    private String status;
}
