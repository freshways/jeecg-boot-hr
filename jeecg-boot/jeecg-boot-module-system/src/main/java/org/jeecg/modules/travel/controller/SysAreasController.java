package org.jeecg.modules.travel.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.travel.entity.SysAreaCity;
import org.jeecg.modules.travel.entity.SysAreas;
import org.jeecg.modules.travel.vo.SysAreasPage;
import org.jeecg.modules.travel.service.ISysAreasService;
import org.jeecg.modules.travel.service.ISysAreaCityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 地区类别
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
@Api(tags="地区类别")
@RestController
@RequestMapping("/travel/sysAreas")
@Slf4j
public class SysAreasController {
	@Autowired
	private ISysAreasService sysAreasService;
	@Autowired
	private ISysAreaCityService sysAreaCityService;
	
	/**
	 * 分页列表查询
	 *
	 * @param sysAreas
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "地区类别-分页列表查询")
	@ApiOperation(value="地区类别-分页列表查询", notes="地区类别-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SysAreas sysAreas,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SysAreas> queryWrapper = QueryGenerator.initQueryWrapper(sysAreas, req.getParameterMap());
		Page<SysAreas> page = new Page<SysAreas>(pageNo, pageSize);
		IPage<SysAreas> pageList = sysAreasService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param sysAreasPage
	 * @return
	 */
	@AutoLog(value = "地区类别-添加")
	@ApiOperation(value="地区类别-添加", notes="地区类别-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SysAreasPage sysAreasPage) {
		SysAreas sysAreas = new SysAreas();
		BeanUtils.copyProperties(sysAreasPage, sysAreas);
		sysAreasService.saveMain(sysAreas, sysAreasPage.getSysAreaCityList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param sysAreasPage
	 * @return
	 */
	@AutoLog(value = "地区类别-编辑")
	@ApiOperation(value="地区类别-编辑", notes="地区类别-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SysAreasPage sysAreasPage) {
		SysAreas sysAreas = new SysAreas();
		BeanUtils.copyProperties(sysAreasPage, sysAreas);
		SysAreas sysAreasEntity = sysAreasService.getById(sysAreas.getId());
		if(sysAreasEntity==null) {
			return Result.error("未找到对应数据");
		}
		sysAreasService.updateMain(sysAreas, sysAreasPage.getSysAreaCityList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "地区类别-通过id删除")
	@ApiOperation(value="地区类别-通过id删除", notes="地区类别-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		sysAreasService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "地区类别-批量删除")
	@ApiOperation(value="地区类别-批量删除", notes="地区类别-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.sysAreasService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "地区类别-通过id查询")
	@ApiOperation(value="地区类别-通过id查询", notes="地区类别-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SysAreas sysAreas = sysAreasService.getById(id);
		if(sysAreas==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(sysAreas);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "地区通过主表ID查询")
	@ApiOperation(value="地区主表ID查询", notes="地区-通主表ID查询")
	@GetMapping(value = "/querySysAreaCityByMainId")
	public Result<?> querySysAreaCityListByMainId(@RequestParam(name="id",required=true) String id) {
		List<SysAreaCity> sysAreaCityList = sysAreaCityService.selectByMainId(id);
		return Result.ok(sysAreaCityList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param sysAreas
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SysAreas sysAreas) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<SysAreas> queryWrapper = QueryGenerator.initQueryWrapper(sysAreas, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<SysAreas> queryList = sysAreasService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<SysAreas> sysAreasList = new ArrayList<SysAreas>();
      if(oConvertUtils.isEmpty(selections)) {
          sysAreasList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          sysAreasList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<SysAreasPage> pageList = new ArrayList<SysAreasPage>();
      for (SysAreas main : sysAreasList) {
          SysAreasPage vo = new SysAreasPage();
          BeanUtils.copyProperties(main, vo);
          List<SysAreaCity> sysAreaCityList = sysAreaCityService.selectByMainId(main.getId());
          vo.setSysAreaCityList(sysAreaCityList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "地区类别列表");
      mv.addObject(NormalExcelConstants.CLASS, SysAreasPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("地区类别数据", "导出人:"+sysUser.getRealname(), "地区类别"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<SysAreasPage> list = ExcelImportUtil.importExcel(file.getInputStream(), SysAreasPage.class, params);
              for (SysAreasPage page : list) {
                  SysAreas po = new SysAreas();
                  BeanUtils.copyProperties(page, po);
                  sysAreasService.saveMain(po, page.getSysAreaCityList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
