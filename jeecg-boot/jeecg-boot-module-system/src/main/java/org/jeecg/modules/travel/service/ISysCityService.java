package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.SysCity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 城市表
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
public interface ISysCityService extends IService<SysCity> {

}
