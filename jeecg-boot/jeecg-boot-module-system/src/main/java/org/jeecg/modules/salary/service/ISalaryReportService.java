package org.jeecg.modules.salary.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.salary.entity.SalaryReport;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 薪资报表
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
public interface ISalaryReportService extends IService<SalaryReport> {

    void  editStatus();

    List<SalaryReport>  getList();

    /**
     * 获取表头
     * @return
     */
    JSONObject reportHead();

    /**
     * 获取分页数据
     * @param realname
     * @param reportId
     * @param currPage
     * @param pageSize
     * @return
     */
    List<Map<String, Object>> getPage(String realname,String reportId, int currPage, int pageSize);
}
