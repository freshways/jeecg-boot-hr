package org.jeecg.modules.travel.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 飞机差旅标准
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
@Data
@TableName("travel_plane")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="travel_plane对象", description="飞机差旅标准")
public class TravelPlane implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**职位*/
	@Excel(name = "职位", width = 15, dictTable = "sys_position", dicText = "name", dicCode = "code")
	@Dict(dictTable = "sys_position", dicText = "name", dicCode = "code")
    @ApiModelProperty(value = "职位")
    private String position;
	/**级别*/
	@Excel(name = "级别", width = 15, dicCode = "aircraft_space")
	@Dict(dicCode = "aircraft_space")
    @ApiModelProperty(value = "级别")
    private String level;
	/**超标管控*/
	@Excel(name = "超标管控", width = 15, dicCode = "controls")
	@Dict(dicCode = "controls")
    @ApiModelProperty(value = "超标管控")
    private String controls;
}
