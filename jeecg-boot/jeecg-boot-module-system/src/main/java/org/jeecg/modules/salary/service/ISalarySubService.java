package org.jeecg.modules.salary.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.salary.entity.SalaryFields;

import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/19
 */
public interface ISalarySubService {


    /**
     * 表中添加字段
     *
     * @param fieldName
     * @return
     */
    boolean insertField(String fieldName);

    /**
     * 删除表中某一字段
     *
     * @param fieldName
     * @return
     */
    boolean deleteField(String fieldName);

    /**
     * 更新录入数据
     *
     * @param map
     */
    boolean updateSalarySub(Map map);


    /**
     * 添加录入数据
     *
     * @param map
     */
    boolean insertSalarySub(Map map);


    /**
     * 根据ID查询
     * @param id
     * @return
     */
    Map getById(String id);


    List getAll();

    /**
     * 根据公式计算数据
     * @param map
     * @return
     */
    List<SalaryFields> getCalculation(Map<String,String> map);

    /**
     * 根据用户ID和reportID获取上报初始数据
     * @param userId
     * @param reportId
     * @return
     */
    List<SalaryFields> getSubInfo(String userId,String reportId);

}