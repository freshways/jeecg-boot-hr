package org.jeecg.modules.salary.service;

import org.jeecg.modules.salary.entity.SysSalary;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 薪资表
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
public interface ISysSalaryService extends IService<SysSalary> {


    /**
     * 根据用户ID和报表ID查询
     * @param userId
     * @param reportId
     * @return
     */
    SysSalary getByUserId(String userId,String reportId);

    /**
     * 根据用户ID查询
     * @param userId
     * @return
     */
    List  selectByUserId(String userId);

}
