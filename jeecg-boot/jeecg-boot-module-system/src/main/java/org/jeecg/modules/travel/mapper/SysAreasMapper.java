package org.jeecg.modules.travel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.travel.entity.SysAreas;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 地区类别
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
public interface SysAreasMapper extends BaseMapper<SysAreas> {

}
