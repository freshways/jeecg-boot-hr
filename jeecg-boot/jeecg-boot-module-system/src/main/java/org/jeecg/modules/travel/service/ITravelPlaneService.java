package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.TravelPlane;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.travel.vo.MyPlaneVo;
import org.jeecg.modules.travel.vo.MyTrainVo;

import java.util.List;

/**
 * @Description: 飞机差旅标准
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
public interface ITravelPlaneService extends IService<TravelPlane> {
    List<MyPlaneVo> getMyPlane();
}
