package org.jeecg.modules.salary.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 薪资管理
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
@Data
@TableName("salary_fields")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="salary_fields对象", description="薪资管理")
public class SalaryFields implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**字段编码（用于生成表字段）*/
	@Excel(name = "字段编码（用于生成表字段）", width = 15)
    @ApiModelProperty(value = "字段编码（用于生成表字段）")
    private String fieldCode;
	/**字段名称*/
	@Excel(name = "字段名称", width = 15)
    @ApiModelProperty(value = "字段名称")
    private String fieldName;
	/**字段默认值*/
	@Excel(name = "字段默认值", width = 15)
    @ApiModelProperty(value = "字段默认值")
    private String fieldDefault;
	/**项目是否显示，1显示，0不显示*/
	@Excel(name = "项目是否显示", width = 15)
    @ApiModelProperty(value = "项目是否显示，1显示，0不显示")
    private String fieldShow;
	/**项目赋值，1默认值，2数据源*/
	@Excel(name = "项目赋值，1默认值，2数据源", width = 15)
    @ApiModelProperty(value = "项目赋值，1默认值，2数据源")
    private String fieldDefaultSet;
	/**数据类型，1数字，2字符，3日期，4时间*/
	@Excel(name = "数据类型，1数字，2字符，3日期，4时间", width = 15, dicCode = "data_type")
	@Dict(dicCode = "data_type")
    @ApiModelProperty(value = "数据类型，1数字，2字符，3日期，4时间")
    private String fieldType;
	/**数据格式，只针对数字类型，1四舍五入，2舍小取整，3进一取整*/
	@Excel(name = "数据格式，只针对数字类型，1四舍五入，2舍小取整，3进一取整", width = 15, dicCode = "data_format")
	@Dict(dicCode = "data_format")
    @ApiModelProperty(value = "数据格式，只针对数字类型，1四舍五入，2舍小取整，3进一取整")
    private String fieldFormat;
	/**小数位*/
	@Excel(name = "小数位", width = 15)
    @ApiModelProperty(value = "小数位")
    private Integer fieldDecimal;
	/**数据源*/
	@Excel(name = "数据源", width = 15)
    @ApiModelProperty(value = "数据源")
    private String fieldSource;
	/**是否参与计算公式，1是*/
	@Excel(name = "是否参与计算公式，1是", width = 15)
    @ApiModelProperty(value = "是否参与计算公式，1是")
    private String isCount;
	/**序号*/
	@Excel(name = "序号", width = 15)
    @ApiModelProperty(value = "序号")
    private Integer fieldSort;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String fieldRemark;
	/**项目是否有效，1有效，0无效*/
	@Excel(name = "项目是否有效，1有效，0无效", width = 15)
    @ApiModelProperty(value = "项目是否有效，1有效，0无效")
    private String fieldCheck;
	/**删除时间*/
	@Excel(name = "删除时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "删除时间")
    private Date deletedAt;
	/**父级ID*/
	@Excel(name = "父级ID", width = 15)
    @ApiModelProperty(value = "父级ID")
    private String fieldParent;
	/**是否有子项，0没有*/
	@Excel(name = "是否有子项，0没有", width = 15)
    @ApiModelProperty(value = "是否有子项，0没有")
    private String hasChildren;

	private  transient  String isReport;

	private transient List<SalaryFields> children;
}
