package org.jeecg.modules.travel.mapper;

import java.util.List;
import org.jeecg.modules.travel.entity.SysAreaCity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
public interface SysAreaCityMapper extends BaseMapper<SysAreaCity> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<SysAreaCity> selectByMainId(@Param("mainId") String mainId);
}
