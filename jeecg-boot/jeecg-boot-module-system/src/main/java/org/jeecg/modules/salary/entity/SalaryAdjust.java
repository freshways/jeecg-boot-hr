package org.jeecg.modules.salary.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 薪资调整
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
@Data
@TableName("salary_adjust")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="salary_adjust对象", description="薪资调整")
public class SalaryAdjust implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**被调整人ID*/
	@Excel(name = "被调整人", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "username")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "username")
    @ApiModelProperty(value = "被调整人ID")
    private String userId;
	/**调整项目ID*/
	@Excel(name = "调整项目", width = 15, dictTable = "salary_fields", dicText = "field_name", dicCode = "id")
	@Dict(dictTable = "salary_fields", dicText = "field_name", dicCode = "id")
    @ApiModelProperty(value = "调整项目ID")
    private String fieldId;
	/**调整幅度*/
	@Excel(name = "调整幅度", width = 15)
    @ApiModelProperty(value = "调整幅度")
    private String adjustValue;
	/**调整方式,1上调，2下调*/
	@Excel(name = "调整方式", width = 15, dicCode = "adjust_type")
	@Dict(dicCode = "adjust_type")
    @ApiModelProperty(value = "调整方式,1上调，2下调")
    private String adjustType;
	/**调整前薪资*/
	@Excel(name = "调整前薪资", width = 15)
    @ApiModelProperty(value = "调整前薪资")
    private String fieldDefaultOld;
	/**调整hou薪资*/
	@Excel(name = "调整薪后资", width = 15)
    @ApiModelProperty(value = "调整hou薪资")
    private String fieldDefaultNew;
	/**调整原因*/
	@Excel(name = "调整原因", width = 15)
    @ApiModelProperty(value = "调整原因")
    private String adjustReason;
	/**关联流程数据*/
	//@Excel(name = "关联流程数据", width = 15)
    @ApiModelProperty(value = "关联流程数据")
    private String extraFields;
	/**调整来源，1-模块，2-流程*/
	//@Excel(name = "调整来源，1-模块，2-流程", width = 15)
    @ApiModelProperty(value = "调整来源，1-模块，2-流程")
    private String adjustFrom;
}
