package org.jeecg.modules.salary.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.entity.SysSalary;
import org.jeecg.modules.salary.service.ISalaryAdjustService;
import org.jeecg.modules.salary.service.ISalaryFieldsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.salary.service.ISalarySubService;
import org.jeecg.modules.salary.service.ISysSalaryService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 薪资管理
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
@Api(tags="薪资管理")
@RestController
@RequestMapping("/salary/salaryFields")
@Slf4j
public class SalaryFieldsController extends JeecgController<SalaryFields, ISalaryFieldsService> {
	@Autowired
	private ISalaryFieldsService salaryFieldsService;

	@Autowired
	private ISalarySubService salarySubService;




	/**
	 * 分页列表查询
	 *
	 * @param salaryFields
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "薪资管理-分页列表查询")
	@ApiOperation(value="薪资管理-分页列表查询", notes="薪资管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SalaryFields salaryFields,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SalaryFields> queryWrapper = QueryGenerator.initQueryWrapper(salaryFields, req.getParameterMap());
		queryWrapper.isNull("deleted_at");
		Page<SalaryFields> page = new Page<SalaryFields>(pageNo, pageSize);
		IPage<SalaryFields> pageList = salaryFieldsService.page(page, queryWrapper);
		List<SalaryFields>  list = pageList.getRecords();
		for(SalaryFields salary :list ){
			if(CommonConstant.STATUS_1.equals(salary.getHasChildren())){
				salary.setFieldDefault("含有子项");
			}else if(CommonConstant.STATUS_2.equals(salary.getFieldDefaultSet())){
				salary.setFieldDefault("计算获值");
			}
		}
		pageList.setRecords(list);
		return Result.ok(pageList);
	}


	 /**
	  * 分页列表查询
	  *
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "薪资管理-列表查询")
	 @ApiOperation(value="薪资管理-列表查询", notes="薪资管理-列表查询")
	 @GetMapping(value = "/queryList")
	 public Result<?> queryList( HttpServletRequest req){
		 List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
		 for(SalaryFields salaryFields : salaryFieldsList){
			 List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
			 if (salaryFieldsList1.size()>0) {
				 salaryFields.setChildren(salaryFieldsList1);
			 }
		 }
		 return Result.ok(salaryFieldsList);
	 }

	
	/**
	 *   添加
	 *
	 * @param salaryFields
	 * @return
	 */
	@AutoLog(value = "薪资管理-添加")
	@ApiOperation(value="薪资管理-添加", notes="薪资管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SalaryFields salaryFields) {
		String result  = salaryFieldsService.insert(salaryFields);
		if(CommonConstant.STATUS_0.equals(result)){
			return Result.ok("公式错误");
		}
		return Result.ok("添加成功");
	}
	
	/**
	 *  编辑
	 *
	 * @param salaryFields
	 * @return
	 */
	@AutoLog(value = "薪资管理-编辑")
	@ApiOperation(value="薪资管理-编辑", notes="薪资管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SalaryFields salaryFields) {
		String result  = salaryFieldsService.update(salaryFields);
		if(CommonConstant.STATUS_0.equals(result)){
			return Result.ok("公式错误");
		}
		return Result.ok("编辑成功");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "薪资管理-通过id删除")
	@ApiOperation(value="薪资管理-通过id删除", notes="薪资管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		String code = salaryFieldsService.deleteById(id);
		if(CommonConstant.STATUS_0.equals(code)){
			return Result.error("删除失败,含有子项");
		}
		return Result.ok("删除成功");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "薪资管理-批量删除")
	@ApiOperation(value="薪资管理-批量删除", notes="薪资管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		List<String> list =  Arrays.asList(ids.split(","));
		String  code  = salaryFieldsService.deleteByIds(list);
		if(CommonConstant.STATUS_0.equals(code)){
			return Result.error("删除失败,含有子项");
		}
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "薪资管理-通过id查询")
	@ApiOperation(value="薪资管理-通过id查询", notes="薪资管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SalaryFields salaryFields = salaryFieldsService.getById(id);
		if(salaryFields==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(salaryFields);
	}


	 /**
	  * 查询上报报表
	  *
	  * @return
	  */
	 @AutoLog(value = "薪资管理-通过id查询")
	 @ApiOperation(value="薪资管理-通过id查询", notes="薪资管理-通过id查询")
	 @GetMapping(value = "/listAll")
	 public Result<?> listAll() {
		 LambdaQueryWrapper<SalaryFields> queryWrapper = new LambdaQueryWrapper();
		 queryWrapper.isNull(SalaryFields::getDeletedAt);
		 //是否有子节点
		 queryWrapper.eq(SalaryFields::getHasChildren,CommonConstant.STATUS_0);
		 List<SalaryFields> salaryFields = salaryFieldsService.list(queryWrapper);
		 if(salaryFields==null) {
			 return Result.error("未找到对应数据");
		 }
		 return Result.ok(salaryFields);
	 }


	 /**
	  * 获取最大fieldSort
	  *
	  * @return
	  */
	 @AutoLog(value = "薪资管理-获取最大fieldSort")
	 @ApiOperation(value="薪资管理-获取最大fieldSort", notes="薪资管理-获取最大fieldSort")
	 @GetMapping(value = "/getMaxNumber")
	 public Result<?> getResult(){
		String num = salaryFieldsService.getMaxFieldSort();
		return Result.ok(num);
	 }


 }
