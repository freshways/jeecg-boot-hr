package org.jeecg.modules.travel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.travel.entity.TravelTrain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.travel.vo.MyHotelVo;

/**
 * @Description: 火车差旅规则
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface TravelTrainMapper extends BaseMapper<TravelTrain> {

    public List<TravelTrain> selectByPostCode(@Param("postCode") String postCode);

}
