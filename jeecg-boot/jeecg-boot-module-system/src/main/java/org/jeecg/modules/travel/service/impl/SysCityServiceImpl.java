package org.jeecg.modules.travel.service.impl;

import org.jeecg.modules.travel.entity.SysCity;
import org.jeecg.modules.travel.mapper.SysCityMapper;
import org.jeecg.modules.travel.service.ISysCityService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 城市表
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
@Service
public class SysCityServiceImpl extends ServiceImpl<SysCityMapper, SysCity> implements ISysCityService {

}
