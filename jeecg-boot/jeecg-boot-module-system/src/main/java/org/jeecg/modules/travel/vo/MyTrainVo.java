package org.jeecg.modules.travel.vo;

import lombok.Data;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
@Data
public class MyTrainVo {

    private String seats;
    private String controls;
}
