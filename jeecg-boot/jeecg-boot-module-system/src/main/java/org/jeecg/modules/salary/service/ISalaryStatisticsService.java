package org.jeecg.modules.salary.service;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/21
 */
public interface ISalaryStatisticsService {

    /**
     * 标题
     * @return
     */
    JSONObject mySalaryStaticHead(String userId);

    /**
     * 分页
     * @param currPage 用户名
     * @param currPage 当前页
     * @param pageSize  条数
     * @return
     */
    List<HashMap<String, Object>> getPage(String userId,String realname ,int currPage, int pageSize);


    List<Map<String, Object>> getList(String userId,String realname);

    /**
     * 求总和
     * @return
     */
    Map getCount();

}
