package org.jeecg.modules.salary.controller;


import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.entity.SysSalary;
import org.jeecg.modules.salary.service.ISalaryAdjustService;
import org.jeecg.modules.salary.service.ISalaryFieldsService;
import org.jeecg.modules.salary.service.ISalarySubService;
import org.jeecg.modules.salary.service.ISysSalaryService;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/20
 */
@Api(tags="薪资上报")
@RestController
@RequestMapping("/salary/salarySub")
@Slf4j
public class SalarySubController {


    @Autowired
    ISysSalaryService  sysSalaryService;
    @Autowired
    ISalarySubService salarySubService;
    @Autowired
    ISalaryFieldsService salaryFieldsService;

    @Autowired
    ISysUserService sysUserService;

    /**
     *  添加或更新
     *
     * @param params
     * @return
     */
    @AutoLog(value = "薪资上报-添加或更新")
    @ApiOperation(value="薪资上报-添加或更新", notes="薪资上报-添加或更新")
    @PostMapping(value = "/addOrUpdate")
    public Result<?> addOrUpdate(@RequestBody Map<String,String> params) {
        boolean flag = false;
        String userId = params.get("userId");
        String reportId = params.get("reportId");

       SysUser  sysUser = sysUserService.getUserByName(userId);

        SysSalary  sysSalary = new SysSalary();
        //查询是否已上报
        sysSalary = sysSalaryService.getByUserId(userId,reportId);
        if(Objects.isNull(sysSalary)){
            SysSalary  sysSalary1 = new SysSalary();
            sysSalary1.setReportId(reportId);
            sysSalary1.setUserId(userId);
            if(Objects.nonNull(sysUser)){
                sysSalary1.setUserName(sysUser.getRealname());
            }
            //保存信息信息
            sysSalaryService.save(sysSalary1);
            //保存
            flag = salarySubService.insertSalarySub(data(sysSalary1,params));
        }else{
            flag = salarySubService.updateSalarySub(data(sysSalary,params));
        }
        return Result.ok(flag);
    }


    /**
     * 处理map集合
     * @param sysSalary
     * @param params
     * @return
     */
    public Map<String,String> data(SysSalary  sysSalary,Map<String,String> params){
        params.put("id",sysSalary.getId());
        params.remove("reportId");
        params.remove("userId");
        String[] keys = new String[params.size()];
        Set<String> sset = params.keySet();
        int i = 0;
        for (String os : sset) {
            keys[i++] = os;
        }
        Map map = new HashMap<>();
        map.put("keys", keys);
        map.put("params", params);
        return map;
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "薪资上报-通过id查询")
    @ApiOperation(value="薪资上报-通过id查询", notes="薪资上报-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
        Map sysSalary = salarySubService.getById(id);
        if(sysSalary==null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(sysSalary);
    }

    /**
     * 获取全部数据
     *
     * @return
     */
    @AutoLog(value = "薪资上报-获取全部数据")
    @ApiOperation(value="薪资上报-获取全部数据", notes="薪资上报-获取全部数据")
    @GetMapping(value = "/getAll")
    public Result<?> getAll() {
        List list  = salarySubService.getAll();
        if(list==null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(list);
    }

    /**
     * 通过userId和reportId查询上报信息
     *
     * @param userId
     * @param reportId
     * @return
     */
    @AutoLog(value = "薪资上报-通过userId和reportId查询上报信息")
    @ApiOperation(value="薪资录入-通过userId和reportId查询上报信息", notes="薪资录入-通过userId和reportId查询上报信息")
    @GetMapping(value = "/getByUserAndReport")
    public Result<?> getByUserAndReport(@RequestParam(name="userId",required=true) String userId,
                                   @RequestParam(name="reportId",required=true) String reportId) {
       List<SalaryFields> fieldsList = salarySubService.getSubInfo(userId,reportId);
       return Result.ok(fieldsList);
    }

    /**
     * 获取公式求和数据
     *
     * @return
     */
    @AutoLog(value = "薪资上报-根据公式处理数据数据求和")
    @ApiOperation(value="薪资上报-根据公式处理数据数据求和", notes="薪资上报-根据公式处理数据数据求和")
    @PostMapping(value = "/getCalculation")
    public Result<?> getCalculation(@RequestBody Map map) {
        List<SalaryFields> salaryFieldsList = salarySubService.getCalculation(map);
        return Result.ok(salaryFieldsList);
    }

}
