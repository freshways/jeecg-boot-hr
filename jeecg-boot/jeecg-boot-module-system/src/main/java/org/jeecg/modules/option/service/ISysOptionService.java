package org.jeecg.modules.option.service;

import org.jeecg.modules.option.entity.SysOption;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 期权管理
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
public interface ISysOptionService extends IService<SysOption> {


    /**
     * 查询未到期限的期权
     * @return
     */
    List<SysOption> getList();
}
