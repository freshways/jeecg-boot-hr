package org.jeecg.modules.option.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.option.entity.SysOption;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 期权管理
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
public interface SysOptionMapper extends BaseMapper<SysOption> {

}
