package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.TravelTrain;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.travel.vo.MyTrainVo;

import java.util.List;

/**
 * @Description: 火车差旅规则
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface ITravelTrainService extends IService<TravelTrain> {
    List<MyTrainVo> getMyTrains();
}
