package org.jeecg.modules.travel.service.impl;

import org.jeecg.modules.travel.entity.SysAreas;
import org.jeecg.modules.travel.entity.SysAreaCity;
import org.jeecg.modules.travel.mapper.SysAreaCityMapper;
import org.jeecg.modules.travel.mapper.SysAreasMapper;
import org.jeecg.modules.travel.service.ISysAreasService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 地区类别
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
@Service
public class SysAreasServiceImpl extends ServiceImpl<SysAreasMapper, SysAreas> implements ISysAreasService {

	@Autowired
	private SysAreasMapper sysAreasMapper;
	@Autowired
	private SysAreaCityMapper sysAreaCityMapper;
	
	@Override
	@Transactional
	public void saveMain(SysAreas sysAreas, List<SysAreaCity> sysAreaCityList) {
		sysAreasMapper.insert(sysAreas);
		if(sysAreaCityList!=null && sysAreaCityList.size()>0) {
			for(SysAreaCity entity:sysAreaCityList) {
				//外键设置
				entity.setCityType(sysAreas.getId());
				sysAreaCityMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(SysAreas sysAreas,List<SysAreaCity> sysAreaCityList) {
		sysAreasMapper.updateById(sysAreas);
		
		//1.先删除子表数据
		sysAreaCityMapper.deleteByMainId(sysAreas.getId());
		
		//2.子表数据重新插入
		if(sysAreaCityList!=null && sysAreaCityList.size()>0) {
			for(SysAreaCity entity:sysAreaCityList) {
				//外键设置
				entity.setCityType(sysAreas.getId());
				sysAreaCityMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		sysAreaCityMapper.deleteByMainId(id);
		sysAreasMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			sysAreaCityMapper.deleteByMainId(id.toString());
			sysAreasMapper.deleteById(id);
		}
	}
	
}
