package org.jeecg.modules.salary.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.excel.ExcelUtils;
import org.jeecg.common.excel.ExportField;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.entity.SalaryReport;
import org.jeecg.modules.salary.service.ISalaryFieldsService;
import org.jeecg.modules.salary.service.ISalaryReportService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 薪资报表
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
@Api(tags="薪资报表")
@RestController
@RequestMapping("/salary/salaryReport")
@Slf4j
public class SalaryReportController extends JeecgController<SalaryReport, ISalaryReportService> {
	@Autowired
	private ISalaryReportService salaryReportService;
	@Autowired
	ISalaryFieldsService salaryFieldsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param salaryReport
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "薪资报表-分页列表查询")
	@ApiOperation(value="薪资报表-分页列表查询", notes="薪资报表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SalaryReport salaryReport,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SalaryReport> queryWrapper = QueryGenerator.initQueryWrapper(salaryReport, req.getParameterMap());
		Page<SalaryReport> page = new Page<SalaryReport>(pageNo, pageSize);
		IPage<SalaryReport> pageList = salaryReportService.page(page, queryWrapper);
		return Result.ok(pageList);
	}


	 /**
	  * 分页列表查询
	  *
	  * @param salaryReport
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "薪资录入-分页列表查询")
	 @ApiOperation(value="薪资报表-分页列表查询", notes="薪资报表-分页列表查询")
	 @GetMapping(value = "/subList")
	 public Result<?> queryPageSubList(SalaryReport salaryReport,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 QueryWrapper<SalaryReport> queryWrapper = QueryGenerator.initQueryWrapper(salaryReport, req.getParameterMap());
		 queryWrapper.eq("status",CommonConstant.STATUS_1);
		 Page<SalaryReport> page = new Page<SalaryReport>(pageNo, pageSize);
		 IPage<SalaryReport> pageList = salaryReportService.page(page, queryWrapper);
		 return Result.ok(pageList);
	 }
	
	/**
	 *   添加
	 *
	 * @param salaryReport
	 * @return
	 */
	@AutoLog(value = "薪资报表-添加")
	@ApiOperation(value="薪资报表-添加", notes="薪资报表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SalaryReport salaryReport) {
		salaryReport.setStatus(CommonConstant.STATUS_1);
		salaryReportService.save(salaryReport);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param salaryReport
	 * @return
	 */
	@AutoLog(value = "薪资报表-编辑")
	@ApiOperation(value="薪资报表-编辑", notes="薪资报表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SalaryReport salaryReport) {
		salaryReportService.updateById(salaryReport);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "薪资报表-通过id删除")
	@ApiOperation(value="薪资报表-通过id删除", notes="薪资报表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		salaryReportService.removeById(id);
		return Result.ok("删除成功!");
	}

	 /**
	  *   通过id删除
	  *
	  * @param id
	  * @return
	  */
	 @AutoLog(value = "薪资报表-终止")
	 @ApiOperation(value="薪资报表-终止", notes="薪资报表-终止")
	 @DeleteMapping(value = "/editStatus")
	 public Result<?> editStatus(@RequestParam(name="id",required=true) String id) {
		 SalaryReport salaryReport = salaryReportService.getById(id);
		 salaryReport.setStatus(CommonConstant.STATUS_2);
		 salaryReportService.updateById(salaryReport);
		 return Result.ok("修改成功!");
	 }

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "薪资报表-批量删除")
	@ApiOperation(value="薪资报表-批量删除", notes="薪资报表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.salaryReportService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "薪资报表-通过id查询")
	@ApiOperation(value="薪资报表-通过id查询", notes="薪资报表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SalaryReport salaryReport = salaryReportService.getById(id);
		if(salaryReport==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(salaryReport);
	}


	 /**
	  * 获取表头数据
	  * @return
	  */
	 @AutoLog(value = "薪资报表-薪资报表表头")
	 @ApiOperation(value="薪资报表-薪资报表表头", notes="薪资报表-薪资报表表头")
	 @GetMapping(value = "/reportHead")
	 public Result<?> getMyHead() {
	 	 JSONObject json = salaryReportService.reportHead();
		 return Result.ok(json);
	 }


	 /**
	  * 获取我的分页列表查询
	  *
	  * @param pageNo
	  * @param pageSize
	  * @return
	  */
	 @AutoLog(value = "薪资报表-薪资报表分页列表查询")
	 @ApiOperation(value="薪资报表-薪资报表分页列表查询", notes="薪资报表-薪资报表分页列表查询")
	 @GetMapping(value = "/pageList")
	 public Result<?> pageList(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
							       @RequestParam(name="reportId", defaultValue="10") String reportId,
							       @RequestParam(name="realname", defaultValue="") String realname) {
		 JSONObject json = new JSONObject();
		 List<Map<String,Object>> pageList = salaryReportService.getPage(realname,reportId,pageNo, pageSize);
		 List<Map<String,Object>> list = salaryReportService.getPage(realname,reportId,0, 0);
		 int a = list.size()/pageSize;
		 int b = list.size()%pageSize;
		 if(b>0){
			 a = a +1;
		 }
		 json.put("records",pageList);
		 json.put("current",pageNo);
		 json.put("size",pageSize);
		 json.put("pages",a);
		 json.put("total",list.size());
		 return Result.ok(json);
	 }


    /**
    * 导出excel
    *
    * @param response
    * @param reportId
    */
	@AutoLog(value = "薪资报表-薪资导出")
	@ApiOperation(value="薪资报表-薪资导出", notes="薪资报表-薪资导出")
    @RequestMapping(value = "/exportXls")
    public Result<?> exportXls(HttpServletResponse response,
							   @RequestParam(name="reportId") String reportId ,
							   @RequestParam(name="realname") String realname) {
		List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
		List<ExportField> exportFieldList = new ArrayList<>();
		ExportField exportField3 = new ExportField();
		exportField3.setEntityAttrName("user_name");
		exportField3.setFiledChineseName("姓名");
		exportFieldList.add(exportField3);

		ExportField exportField0 = new ExportField();
		exportField0.setEntityAttrName("position");
		exportField0.setFiledChineseName("职位");
		exportFieldList.add(exportField0);

		ExportField exportField2 = new ExportField();
		exportField2.setEntityAttrName("dept_name");
		exportField2.setFiledChineseName("部门");
		exportFieldList.add(exportField2);

		for(SalaryFields salaryFields :salaryFieldsList){
			ExportField exportField = new ExportField();
			exportField.setEntityAttrName(salaryFields.getFieldCode());
			exportField.setFiledChineseName(salaryFields.getFieldName());
			List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
			if (salaryFieldsList1.size()>0) {
				List<ExportField> exportFieldList1 = new ArrayList<>();
				for(SalaryFields salaryFields1 : salaryFieldsList1){
					ExportField exportField1 = new ExportField();
					exportField1.setEntityAttrName(salaryFields1.getFieldCode());
					exportField1.setFiledChineseName(salaryFields1.getFieldName());
					exportFieldList1.add(exportField1);
				}
				exportField.setChildren(exportFieldList1);
			}
			exportFieldList.add(exportField);
		}

		List<Map<String,Object>> list = salaryReportService.getPage(realname,reportId,0, 0);
		ExcelUtils.DynamicExcel("薪资报表" + IdUtil.fastSimpleUUID() + ".xls", "薪资报表",
				"sheet1", exportFieldList, response, list);
		return Result.ok("导出成功");
    }


}
