package org.jeecg.modules.salary.controller;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.excel.ExcelUtils;
import org.jeecg.common.excel.ExportField;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.salary.entity.SalaryFields;
import org.jeecg.modules.salary.service.ISalaryFieldsService;
import org.jeecg.modules.salary.service.ISalaryStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/21
 */
@Api(tags="薪资统计")
@RestController
@RequestMapping("/salary/salaryStatist")
@Slf4j
public class SalaryStatisticsController {


    @Autowired
    private ISalaryStatisticsService  salaryStatisticsService;

    @Autowired
    private ISalaryFieldsService salaryFieldsService;

    /**
     * 获取表头数据
     * @return
     */
    @AutoLog(value = "薪资统计-获取我的表头数据")
    @ApiOperation(value="薪资统计-获取我的表头数据", notes="薪资统计-获取我的表头数据")
    @GetMapping(value = "/getMyHead")
    public Result<?> getMyHead() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        JSONObject json = salaryStatisticsService.mySalaryStaticHead(sysUser.getUsername());
        return Result.ok(json);
    }


    /**
     * 获取表头数据
     * @return
     */
    @AutoLog(value = "薪资统计-获取表头数据")
    @ApiOperation(value="薪资统计-获取表头数据", notes="薪资统计-获取表头数据")
    @GetMapping(value = "/getHead")
    public Result<?> getHead() {
        JSONObject json  = salaryStatisticsService.mySalaryStaticHead(null);
        return Result.ok(json);
    }


    /**
     * 获取我的分页列表查询
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "薪资统计-我的薪资分页列表查询")
    @ApiOperation(value="薪资统计-我的薪资分页列表查询", notes="薪资统计-我的薪资分页列表查询")
    @GetMapping(value = "/mySalaryList")
    public Result<?> mySalaryList(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        JSONObject json = new JSONObject();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        List<HashMap<String,Object>> pageList = salaryStatisticsService.getPage(sysUser.getUsername(),null,pageNo, pageSize);
        List<Map<String, Object>> list = salaryStatisticsService.getList(sysUser.getUsername(),null);
        int a = list.size()/pageSize;
        int b = list.size()%pageSize;
        if(b>0){
            a = a +1;
        }
        json.put("records",pageList);
        json.put("current",pageNo);
        json.put("size",pageSize);
        json.put("pages",a);
        json.put("total",list.size());
        return Result.ok(json);
    }


    /**
     * 获取分页列表查询
     *
     * @param pageNo
     * @param pageSize
     * @return
     */
    @AutoLog(value = "薪资统计-薪资分页列表查询")
    @ApiOperation(value="薪资统计-薪资分页列表查询", notes="薪资统计-薪资分页列表查询")
    @GetMapping(value = "/salaryList")
    public Result<?> salaryList(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                @RequestParam(name="realname",defaultValue = "") String realname) {
        JSONObject json = new JSONObject();
        List<HashMap<String,Object>> pageList = salaryStatisticsService.getPage(null,realname,pageNo, pageSize);
        List<Map<String, Object>> list = salaryStatisticsService.getList(null,realname);
        int a = list.size()/pageSize;
        int b = list.size()%pageSize;
        if(b>0){
            a = a +1;
        }
        json.put("records",pageList);
        json.put("current",pageNo);
        json.put("size",pageSize);
        json.put("pages",a);
        json.put("total",list.size());
        return Result.ok(json);
    }


    @AutoLog(value = "薪资统计-求和")
    @ApiOperation(value="薪资统计-求和", notes="薪资统计-求和")
    @GetMapping(value = "/getCount")
    public Result<?> getCount() {
        Map map = salaryStatisticsService.getCount();
        return Result.ok(map);
    }


    @AutoLog(value = "薪资统计-我的薪资导出")
    @ApiOperation(value="薪资统计-我的薪资导出", notes="薪资统计-我的薪资导出")
    @GetMapping(value = "/myExport")
    public Result<?> export(HttpServletResponse response)  {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
        List<ExportField> exportFieldList = new ArrayList<>();
        ExportField exportField2 = new ExportField();
        exportField2.setEntityAttrName("title");
        exportField2.setFiledChineseName("说明");
        exportFieldList.add(exportField2);

        ExportField exportField3 = new ExportField();
        exportField3.setEntityAttrName("user_name");
        exportField3.setFiledChineseName("姓名");
        exportFieldList.add(exportField3);

        ExportField exportField4 = new ExportField();
        exportField4.setEntityAttrName("sub_date");
        exportField4.setFiledChineseName("上报年月");
        exportFieldList.add(exportField4);

        for(SalaryFields salaryFields :salaryFieldsList){
            ExportField exportField = new ExportField();
            exportField.setEntityAttrName(salaryFields.getFieldCode());
            exportField.setFiledChineseName(salaryFields.getFieldName());
            List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
            if (salaryFieldsList1.size()>0) {
                List<ExportField> exportFieldList1 = new ArrayList<>();
                for(SalaryFields salaryFields1 : salaryFieldsList1){
                    ExportField exportField1 = new ExportField();
                    exportField1.setEntityAttrName(salaryFields1.getFieldCode());
                    exportField1.setFiledChineseName(salaryFields1.getFieldName());
                    exportFieldList1.add(exportField1);
                }
                exportField.setChildren(exportFieldList1);
            }
            exportFieldList.add(exportField);
        }

        List<Map<String, Object>> list = salaryStatisticsService.getList(sysUser.getUsername(),null);
        ExcelUtils.DynamicExcel("薪资报表" + IdUtil.fastSimpleUUID() + ".xls", "我的薪资",
                "sheet1", exportFieldList, response, list);
        return Result.ok("导出成功");
    }


    @AutoLog(value = "薪资统计-薪资导出")
    @ApiOperation(value="薪资统计-薪资导出", notes="薪资统计-薪资导出")
    @GetMapping(value = "/export")
    public Result<?> export(HttpServletResponse response,String realname)  {
        List<SalaryFields> salaryFieldsList = salaryFieldsService.listParentAll();
        List<ExportField> exportFieldList = new ArrayList<>();

        ExportField exportField2 = new ExportField();
        exportField2.setEntityAttrName("title");
        exportField2.setFiledChineseName("说明");
        exportFieldList.add(exportField2);

        ExportField exportField3 = new ExportField();
        exportField3.setEntityAttrName("user_name");
        exportField3.setFiledChineseName("姓名");
        exportFieldList.add(exportField3);

        ExportField exportField4 = new ExportField();
        exportField4.setEntityAttrName("sub_date");
        exportField4.setFiledChineseName("上报年月");
        exportFieldList.add(exportField4);

        for(SalaryFields salaryFields :salaryFieldsList){
            ExportField exportField = new ExportField();
            exportField.setEntityAttrName(salaryFields.getFieldCode());
            exportField.setFiledChineseName(salaryFields.getFieldName());
            List<SalaryFields> salaryFieldsList1 = salaryFieldsService.getByFieldParent(salaryFields.getId());
            if (salaryFieldsList1.size()>0) {
                List<ExportField> exportFieldList1 = new ArrayList<>();
                for(SalaryFields salaryFields1 : salaryFieldsList1){
                    ExportField exportField1 = new ExportField();
                    exportField1.setEntityAttrName(salaryFields1.getFieldCode());
                    exportField1.setFiledChineseName(salaryFields1.getFieldName());
                    exportFieldList1.add(exportField1);
                }
                exportField.setChildren(exportFieldList1);
            }
            exportFieldList.add(exportField);
        }
        List<Map<String, Object>> list = salaryStatisticsService.getList(null,realname);
        ExcelUtils.DynamicExcel("薪资报表" + IdUtil.fastSimpleUUID() + ".xls", "薪资报表",
                "sheet1", exportFieldList, response, list);
        return Result.ok("导出成功");
    }


}
