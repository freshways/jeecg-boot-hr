package org.jeecg.modules.option.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.option.entity.SysOption;
import org.jeecg.modules.option.mapper.SysOptionMapper;
import org.jeecg.modules.option.service.ISysOptionService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 期权管理
 * @Author: jeecg-boot
 * @Date:   2020-08-26
 * @Version: V1.0
 */
@Service
public class SysOptionServiceImpl extends ServiceImpl<SysOptionMapper, SysOption> implements ISysOptionService {

    @Override
    public List<SysOption> getList() {
        LambdaQueryWrapper<SysOption> queryWrapper = new  LambdaQueryWrapper();
        queryWrapper.eq(SysOption::getStatus, CommonConstant.STATUS_0);
        return list(queryWrapper);
    }
}
