package org.jeecg.modules.salary.service.impl;

import org.jeecg.modules.salary.entity.SalaryAdjust;
import org.jeecg.modules.salary.mapper.SalaryAdjustMapper;
import org.jeecg.modules.salary.service.ISalaryAdjustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 薪资调整
 * @Author: jeecg-boot
 * @Date:   2020-08-20
 * @Version: V1.0
 */
@Service
public class SalaryAdjustServiceImpl extends ServiceImpl<SalaryAdjustMapper, SalaryAdjust> implements ISalaryAdjustService {

    @Autowired
    SalaryAdjustMapper salaryAdjustMapper;

    @Override
    public String getMaxMoney(String userId, String fieldid) {
        return salaryAdjustMapper.getMax(userId,fieldid);
    }
}
