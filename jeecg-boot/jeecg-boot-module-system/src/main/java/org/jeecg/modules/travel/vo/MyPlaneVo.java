package org.jeecg.modules.travel.vo;

import lombok.Data;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/26
 */
@Data
public class MyPlaneVo {
    private String level;
    private String controls;
}
