package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.vo.MyTravelVo;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
public interface IMyTravelService {

    MyTravelVo getMyTravel();
}
