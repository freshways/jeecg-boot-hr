package org.jeecg.modules.salary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.salary.entity.SalaryFields;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 薪资管理
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
public interface SalaryFieldsMapper extends BaseMapper<SalaryFields> {

    /**
     * 获取最大的字段
     * @return
     */
    @Select(value = "select sf.field_code from  salary_fields  sf where sf.create_time = (select  max(create_time) from salary_fields)")
    String getMaxField();

    /**
     * 获取最大 fieldSort
     * @return
     */
    @Select(value = " select MAX(field_sort) fieldSort from salary_fields  ")
    String getMaxFieldSort();

    /**
     * @param fieldParent
     * @return
     */
    @Select(value = " select * from salary_fields where field_parent = #{fieldParent} ")
    List<SalaryFields> getListByParent(String fieldParent);
}