package org.jeecg.modules.salary.service;

import org.jeecg.modules.salary.entity.SalaryFields;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 薪资管理
 * @Author: jeecg-boot
 * @Date:   2020-08-19
 * @Version: V1.0
 */
public interface ISalaryFieldsService extends IService<SalaryFields> {


    /**
     * 获取最大Field字段
     * @return
     */
    String getMaxField();

    /**
     * 获取不含子类的数据
     * @return
     */
    List<SalaryFields> list();

    /**
     * 根据用户ID和fieldId查询最新的filedValue
     * @param userId
     * @param fieldId
     * @return
     */
    String getFieldValue(String userId,String fieldId);


    /**
     * 获取所有父类数据
     * @return
     */
    List<SalaryFields> listParentAll();

    /**
     * 根据ID查询子类信息
     * @param id
     * @return
     */
    List<SalaryFields> getByFieldParent(String id);


    /**
     * 添加
     * @param salaryFields
     */
    String  insert(SalaryFields salaryFields);


    /**
     * 获取最大fieldSort
     * @return
     */
    String getMaxFieldSort();


    /**
     * 批量删除
     * @param ids
     * @return
     */
    String deleteByIds(List<String> ids);


    /**
     * 更新
     * @param salaryFields
     */
    String  update(SalaryFields salaryFields);

    /**
     * 根据ID删除数据
     * @param id
     * @return
     */
    String deleteById(String id);

}
