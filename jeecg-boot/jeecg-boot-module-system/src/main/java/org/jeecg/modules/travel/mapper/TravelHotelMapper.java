package org.jeecg.modules.travel.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.travel.entity.TravelHotel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.travel.entity.TravelTrain;
import org.jeecg.modules.travel.vo.MyHotelVo;

/**
 * @Description: 酒店差标
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
public interface TravelHotelMapper extends BaseMapper<TravelHotel> {
    public List<TravelHotel> selectByPostCode(@Param("postCode") String postCode);
    public List<Map<Object,Object>> selectHotelVoByPostCode(@Param("postCode") String postCode);

}
