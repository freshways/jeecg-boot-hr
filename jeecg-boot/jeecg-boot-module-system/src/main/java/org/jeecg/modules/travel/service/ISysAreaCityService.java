package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.SysAreaCity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
public interface ISysAreaCityService extends IService<SysAreaCity> {

	public List<SysAreaCity> selectByMainId(String mainId);
}
