package org.jeecg.modules.option.job;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.message.service.ISysMessageService;
import org.jeecg.modules.option.entity.SysOption;
import org.jeecg.modules.option.service.ISysOptionService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

/**
 * <B>系统名称：</B><BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B><BR>
 * <B>概要说明：</B><BR>
 *
 * @author 中科软科技
 * @since 2020/8/27
 */
@Slf4j
public class OptionJob implements Job {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private ISysOptionService sysOptionService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String toDate = sdf.format(new Date());
        //获取数据
        List<SysOption> sysOptionList = sysOptionService.getList();
        //遍历
        for(SysOption sysOption:sysOptionList){
           String fromDate =  sdf.format(sysOption.getAssignDate());
           String fixedRatio = sysOption.getFixedRatio();
           double sy = subtract(1,Double.valueOf(fixedRatio));
           int  term = sysOption.getTerm();
           int year = returnT(fromDate,toDate);
           if(term == year){
               sysOption.setActualRatio(CommonConstant.STATUS_1);
               sysOption.setStatus(CommonConstant.STATUS_1);
           }else{
              String dd =  txfloat(sy,term);
              double cc = mul(Double.valueOf(dd) , year);
              double actualRatios = add(Double.valueOf(fixedRatio),cc);
              sysOption.setActualRatio(String.valueOf(actualRatios));
           }
            sysOptionService.updateById(sysOption);
        }
    }


    /**
     * 除法运算，保留小数
     * @param a 被除数
     * @param b 除数
     * @return 商
     */
    public static String txfloat(double a,double b) {
        DecimalFormat df=new DecimalFormat("0.0000");//设置保留位数
        return df.format((float)a/b);
    }

    // 进行加法运算
    public static double add(double v1,double v2){

        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }
     // 进行减法运算
     public static double subtract(double v1,double v2){
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

    // 进行乘法运算
    public static double mul(double d1, double d2){
        BigDecimal b1 = new BigDecimal(d1);
        BigDecimal b2 = new BigDecimal(d2);
        return b1.multiply(b2).doubleValue();
    }

    /**
     * 计算年数
     * @param fromDate  开始时间
     * @param toDate    结束时间
     * @return
     */
    public static int returnT(String fromDate,String toDate){
        Period period = Period.between(LocalDate.parse(fromDate), LocalDate.parse(toDate));
        return period.getYears();
    }


    public static void main(String[] args) {
        String fromDate = "2018-10-10";
        String toDate = "2020-08-27";
        String fixedRatio = "0.33";
        String actualRatio = "0.33";
        double  sy = 1 - Double.valueOf(fixedRatio);
        int  term = 3;
        int year = returnT(fromDate,toDate);
        if(term == year){
            actualRatio = "1";
        }else{
            String dd =  txfloat(sy,term);
            double cc = Double.valueOf(dd) * year;
            double actualRatios = Double.valueOf(actualRatio)+cc;
            actualRatio = String.valueOf(actualRatios);
        }
        System.out.println(actualRatio);

//        System.out.println(new Double(0.11));
//        System.out.println(new Double(2.11));
    }

}
