package org.jeecg.modules.travel.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 类描述
 *
 * @author jooe
 * @date 2020/8/16
 */
@Data
public class MyHotelVo {

    private String areaType;
    private BigDecimal max;
    private String hotelStar;
    private String controls;
    private String cityName;
}
