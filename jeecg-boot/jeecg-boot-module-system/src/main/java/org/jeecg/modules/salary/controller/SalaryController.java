package org.jeecg.modules.salary.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.salary.entity.Salary;
import org.jeecg.modules.salary.service.ISalaryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.system.entity.SysPosition;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysPositionService;
import org.jeecg.modules.system.service.ISysUserService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工资信息
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Api(tags="工资信息")
@RestController
@RequestMapping("/salary/salary")
@Slf4j
public class SalaryController extends JeecgController<Salary, ISalaryService> {
	@Autowired
	private ISalaryService salaryService;
	@Autowired
	private ISysPositionService sysPositionService;
	@Autowired
	private ISysUserService sysUserService;

	/**
	 * 分页列表查询
	 *
	 * @param salary
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "工资信息-分页列表查询")
	@ApiOperation(value="工资信息-分页列表查询", notes="工资信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(Salary salary,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Salary> queryWrapper = QueryGenerator.initQueryWrapper(salary, req.getParameterMap());
		Page<Salary> page = new Page<Salary>(pageNo, pageSize);
		IPage<Salary> pageList = salaryService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param salary
	 * @return
	 */
	@AutoLog(value = "工资信息-添加")
	@ApiOperation(value="工资信息-添加", notes="工资信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody Salary salary) {
		salaryService.save(salary);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param salary
	 * @return
	 */
	@AutoLog(value = "工资信息-编辑")
	@ApiOperation(value="工资信息-编辑", notes="工资信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody Salary salary) {
		salaryService.updateById(salary);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "工资信息-通过id删除")
	@ApiOperation(value="工资信息-通过id删除", notes="工资信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		salaryService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "工资信息-批量删除")
	@ApiOperation(value="工资信息-批量删除", notes="工资信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.salaryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "工资信息-通过id查询")
	@ApiOperation(value="工资信息-通过id查询", notes="工资信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		Salary salary = salaryService.getById(id);
		if(salary==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(salary);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param salary
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Salary salary) {
        return super.exportXls(request, salary, Salary.class, "工资信息");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Salary.class);
    }

	 @AutoLog(value = "我的薪酬信息")
	 @ApiOperation(value="我的薪酬信息", notes="我的薪酬信息")
	 @GetMapping(value = "/me")
	 public Result<?> queryByUserId() {
		 Salary salary = salaryService.getMySalary();
		 JSONObject result = new JSONObject();
		 List<Salary> salaryList = new ArrayList<>();
		 salaryList.add(salary);
		 result.put("salary",salaryList);
		 result.put("options",salary.getOptions());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 String position = sysUser.getPost();
		 QueryWrapper<SysPosition> wrapper = new QueryWrapper<>();
		 wrapper.eq("code",position);
		 SysPosition sysPosition = sysPositionService.getOne(wrapper);
		 if (null==sysPosition|| StringUtils.isEmpty(sysPosition.getName())){
			 result.put("postion","");
		 }else{
			 result.put("postion",sysPosition.getName());
		 }
		 SysUser sysUser1 = sysUserService.getById(sysUser.getId());
		 result.put("workNo",sysUser1.getWorkNo());
		 return Result.ok(result);
	 }

}
