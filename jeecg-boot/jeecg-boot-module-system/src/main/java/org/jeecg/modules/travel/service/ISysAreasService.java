package org.jeecg.modules.travel.service;

import org.jeecg.modules.travel.entity.SysAreaCity;
import org.jeecg.modules.travel.entity.SysAreas;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 地区类别
 * @Author: jeecg-boot
 * @Date:   2020-08-15
 * @Version: V1.0
 */
public interface ISysAreasService extends IService<SysAreas> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(SysAreas sysAreas, List<SysAreaCity> sysAreaCityList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(SysAreas sysAreas, List<SysAreaCity> sysAreaCityList);
	
	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);
	
}
