package org.jeecg.modules.travel.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.system.service.ISysDictService;
import org.jeecg.modules.travel.entity.TravelTrain;
import org.jeecg.modules.travel.mapper.TravelTrainMapper;
import org.jeecg.modules.travel.service.ITravelTrainService;
import org.jeecg.modules.travel.vo.MyTrainVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 火车差旅规则
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Service
public class TravelTrainServiceImpl extends ServiceImpl<TravelTrainMapper, TravelTrain> implements ITravelTrainService {
    @Autowired
    private TravelTrainMapper travelTrainMapper;
    @Autowired
    private ISysDictService sysDictService;
    @Override
    public List<MyTrainVo> getMyTrains() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //获取职务编码
        String postCode = sysUser.getPost();
        List<TravelTrain> travelTrainList = travelTrainMapper.selectByPostCode(postCode);
        //获取席位字典信息
        List<DictModel>  seatsModels = sysDictService.queryDictItemsByCode("seats");
        Map<String ,String> seatsMap = new HashMap<String,String>();
        for (DictModel model: seatsModels            ) {
            seatsMap.put(model.getValue(),model.getText());
        }

        List<MyTrainVo> vos =  new ArrayList<>();
        for (TravelTrain travelTrain: travelTrainList            ) {
            MyTrainVo vo = new MyTrainVo();
            String seatsStr = travelTrain.getSeats();
            String[] seats = seatsStr.split(",");
            List<String> list = new ArrayList<>();
            for (String seat:seats        ) {
                list.add(seatsMap.get(seat));
            }
            vo.setSeats(StringUtils.join(list,","));
            vos.add(vo);
        }
        return vos;
    }
}
