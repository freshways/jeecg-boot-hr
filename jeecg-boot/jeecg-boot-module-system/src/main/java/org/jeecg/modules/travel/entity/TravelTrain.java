package org.jeecg.modules.travel.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 火车差旅规则
 * @Author: jeecg-boot
 * @Date:   2020-08-16
 * @Version: V1.0
 */
@Data
@TableName("travel_train")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="travel_train对象", description="火车差旅规则")
public class TravelTrain implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**职级*/
	@Excel(name = "职级", width = 15, dictTable = "sys_position", dicText = "name", dicCode = "code")
	@Dict(dictTable = "sys_position", dicText = "name", dicCode = "code")
    @ApiModelProperty(value = "职级")
    private String position;
	/**席位*/
	@Excel(name = "席位", width = 15, dicCode = "seats")
	@Dict(dicCode = "seats")
    @ApiModelProperty(value = "席位")
    private String seats;
	/**超标管控*/
	@Excel(name = "超标管控", width = 15, dicCode = "controls")
	@Dict(dicCode = "controls")
    @ApiModelProperty(value = "超标管控")
    private String controls;
}
