import Vue from 'vue'
import { axios } from '@/utils/request'


export function editStatus(id) {
  return axios({
    url: '/salary/salaryReport/editStatus?id='+id,
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

