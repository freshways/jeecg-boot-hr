import Vue from 'vue'
import { axios } from '@/utils/request'


export function dicCode() {
  return axios({
    url: '/salary/salaryAdjust/dicCode',
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

